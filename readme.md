## Dependencies 

NodeJS and bower packages must be installed

* Download and install NodeJS
* Install bower globally
* Install gulp (task runner) globally

```
npm install bower -g
npm install gulp -g
```

## Development

To start development, you need to install all node development dependencies and download bower components.
Open terminal and go to project's root folder. Then install nodejs, bower dependencies
 
```
npm install
bower install
```

When all dependencies will have downloaded, run gulp and you can start editing templates and styles

```
gulp
```

## Scripts

Currently all scripts that should be copied from sources (bower components) to public dir must be listed manually
 in gulpfile. Check out configuration object: FILES.DEV.JS and append all required scripts that must be included.
 Also these scripts must be included in layout file.
 
## Templates
 
To generate final templates gulp-assemble is used on the project. 
Please refer to node assemble package for more details

Basically, there are three types of html includes:
- Layouts (used as main layout for pages, currently for this project only one layout file is required)
- Pages (each file in sources/templates/pages folder will generate a newer resulting template, content is inserted into layout {{<body}} section
- Partials (html partials, reusable components for different pages)