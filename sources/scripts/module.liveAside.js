/**
 * @author Roman Morozov <sublimeye.ua@gmail.com>
 * @created on Oct/9/14
 */
(function () {
    'use strict';

    var $liveAside = $('.js-live-aside');

    if (!$liveAside.length) {
        return;
    }

    enableToggableAside();
    enableAdjustableAsideHeight();

    /**
     * Enables aside panel toggle by click
     * @returns {boolean}
     */
    function enableToggableAside() {
        var $liveAsideToggleButton = $('.js-aside-toggle'),
            TOGGLE_CLASS = 'extended';

        if (!$liveAsideToggleButton.length) { return false; }

        $liveAsideToggleButton.on('click', function () {
            $liveAside.toggleClass(TOGGLE_CLASS);
        });
    }

    /**
     * Sets height for aside panel depending on video source height
     * Updates on window resize
     * TODO: refactor to use debounce to prevent extra calculations
     * @returns {boolean}
     */
    function enableAdjustableAsideHeight() {
        var $sourceContainer = $('.js-live-source-container');

        if (!$sourceContainer.length) { return false; }

        $(window).on('resize', function () {
            adjustAsideHeight($sourceContainer);
        });

        adjustAsideHeight($sourceContainer);
    }

    function adjustAsideHeight($sourceContainer) {
        var sourceHeight;
        // subtract height of youtube controls to prevent overlapping
        sourceHeight = $sourceContainer.height();
        sourceHeight = sourceHeight - (sourceHeight / 20);
        $liveAside[0].style.height = sourceHeight + 'px';
    }

})();