(function() {
    'use strict';

    var connect = require('gulp-connect'),
        reload = connect.reload,
        gulp = require('gulp'),
        argv = require('yargs').argv,
        csso = require('gulp-csso'),
        sass = require('gulp-sass'),
        preprocess = require('gulp-preprocess'),
        minifyHtml = require('gulp-minify-html'),
        autoprefixer = require('gulp-autoprefixer'),
        rename = require('gulp-rename'),
        gulpif = require('gulp-if'),
        assemble = require('gulp-assemble'),
        watch = require('gulp-watch'),
        /**
         * Local project environment configuration; This file should not be uploaded into repo
         * Please use ".config-local.sample.js" as a reference sample
         */
        localConfig = require('./.config-local.js');


    /**
         * Gulp tasks and parameters
         *
         * Usage:
         * gulp [task] [--min|--compress] [--server] [--p|--port]
         *
         * Parameter,               Usage           Default             Description
         * ---------------------------------------------------------------------------------------
         * task                     [Local|Server]  Default: 'work'     For local development (watch, no optimization). Use 'build' when deploying
         * --min, --compress        [Local|Server]  Default: false      Turns on|off compression, minifacation, extra comments, etc;
         * --server                 [Local|Build]                       Starts nodejs HTTP server. Can be used only for 'build' task;
         * --d|--debug              [Local|Server]  Default: true       Injects build-specific environments into app (index.html, global var)
         *
         * Local Development Environment:
         * $ gulp
         *
         * Development Environment (Default for dev server, sources not concatenated or minimized)
         * $ gulp build
         *
         * Production Environment (Compressed sources, without comments)
         * $ gulp build --min
         *
         * @isBuild {Boolean} Set to true when gulp is started with "build" task
         */
    var isBuild = (argv['_'].indexOf('build') !== -1),

        DIR_DEV     = 'sources/',
        DIR_BUILD   = 'public/',
        DIR_ASSETS  = 'assets/',

        DIR = {
            DEV: {
                JS: DIR_DEV + 'scripts',
                CSS: DIR_DEV + 'css/',
                TEMPLATES: DIR_DEV + 'templates/',
                LAYOUTS: DIR_DEV + 'templates/layouts/'
            },
            BUILD: {
                JS: DIR_BUILD + 'js/',
                CSS: DIR_BUILD + 'css/',
                HTML: DIR_BUILD
            }
        },

        FILES = {
            DEV: {
                JS: [
                    'sources/bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js',
                    'sources/bower_components/jquery/dist/jquery.js',
                    'sources/bower_components/modernizr/modernizr.js',
                    'sources/scripts/**/*.js'
                ],
                CSS: {
                    ALL: [DIR_DEV + 'styles/**/*.scss'],
                    INDEX: 'index.css'
                },
                TEMPLATES: {
                    PARTIALS: DIR.DEV.TEMPLATES + 'partials/*.hbs',
                    PAGES: DIR.DEV.TEMPLATES + 'pages/*.hbs',
                    LAYOUTS: DIR.DEV.TEMPLATES + 'layouts/*.hbs'
                },
                ASSETS: [
                    DIR_DEV + DIR_ASSETS + '/images/**/*.*',
                    DIR_DEV + DIR_ASSETS + '/fonts/**/*.*',
                    // An Exception for livereload for static html files
                    DIR_BUILD + '**/*.html'
                ]
            },
            BUILD: {
                CLEAN: [DIR_BUILD + 'css/**', DIR_BUILD + 'js/**', DIR_BUILD + '*.html']
            }
        },

        assembleOptions = {
            layoutdir: DIR.DEV.LAYOUTS,
            partials: FILES.DEV.TEMPLATES.PARTIALS,
            data: DIR.DEV + 'data/*.json'
        },

        config = {
            // Compression and optimizations are Disabled by default;
            isCompressed: argv.compress || argv.min || false,
            debug: argv.debug || argv.d || false,
            isLocalServerRequired: isBuild && argv.server
        };

    /**
     * Gulp Main tasks
     * `$ gulp work` (or) simple `$ gulp` is used for local development
     * `$ gulp build --env={test|stage|prod}` creates a build
     * Build can be either "dev" (Files are not concatenated or minimized) or "prod"
     */
    gulp.task('default', ['clean-build'], function () {
        gulp.start('work');
    });

    gulp.task('work', ['styles', 'copy-scripts', 'assemble', 'watch'], function () {
        gulp.start('server');
    });

    /**
     * Watchers
     * SASS -> Prefixer -> Minifier -> CSS
     * index html -> inject vars
     * live-reload when chnaged scripts, templates, images, fonts
     */
    gulp.task('watch', function() {
        gulp.watch(FILES.DEV.CSS.ALL, localConfig.gazeOptions, ['styles']);
        gulp.watch([
            FILES.DEV.TEMPLATES.LAYOUTS,
            FILES.DEV.TEMPLATES.PAGES,
            FILES.DEV.TEMPLATES.PARTIALS
        ], localConfig.gazeOptions, ['assemble']);
        gulp.watch(FILES.DEV.JS, localConfig.gazeOptions, ['copy-scripts']);
        gulp.start('watch-static-files');
    });

    gulp.task('watch-static-files', function () {
        watch({
            glob: FILES.DEV.ASSETS,
            gaze: localConfig.gazeOptions}, function (files) {
            return files.pipe(reload());
        });
    });

    gulp.task('styles', function () {
        return gulp.src('sources/styles/index.scss')
            .pipe(sass({
                outputStyle: (config.isCompressed ? 'compressed' : 'nested'),
                sourceComments: (config.isCompressed ? 'none' : 'normal'),
                errLogToConsole: !isBuild // fail sass task on error
            }))
            .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
            .pipe(rename(FILES.DEV.CSS.INDEX))
            .pipe(gulpif(config.isCompressed, csso()))
            .pipe(gulp.dest(DIR.BUILD.CSS))
            .pipe(gulpif(!isBuild, reload()));
    });

    gulp.task('assemble', function () {
      gulp.src(FILES.DEV.TEMPLATES.PAGES)
        .pipe(assemble(assembleOptions))
        .pipe(gulp.dest(DIR.BUILD.HTML))
        .pipe(gulpif(!isBuild, reload()));
    });

    gulp.task('clean-build', function () {
        var clean = require('gulp-clean');
        return gulp.src(FILES.BUILD.CLEAN, {
            read: false
        }).pipe(clean());
    });

    gulp.task('copy-scripts', function () {
        gulp.src(FILES.DEV.JS)
            .pipe(gulp.dest(DIR.BUILD.JS));
    });

    /**
     * Starts a local server
     * - checks that domain in config is available, otherwise uses localhost
     * - checks if port in conifg is opened, otherwise tries to search for a opened
     */
    gulp.task('server', function () {
        var open = require('gulp-open');
        var rootDir = DIR_BUILD;

        connect.server({
            root: rootDir,
            host: localConfig.host,
            port: localConfig.port,
            livereload: {
                port: localConfig.livereloadPort
            },
            middleware: function () {
                return [];
            }
        });

        if (localConfig.previewInBrowser) {
            gulp.src(rootDir + '/index.html')
                .pipe(open('', {
                    url: 'http://' + localConfig.host + ':' + localConfig.port,
                    app: localConfig.defaultBrowser
                }));
        }

    });

})();
